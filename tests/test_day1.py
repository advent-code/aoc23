import pytest
from treb import day1
from treb import day1b


def test_day1_compute():
    assert day1.compute_oneline("1abc2") == 12
    assert day1.compute_oneline("pqr3stu8vwx") == 38
    assert day1.compute_oneline("a1b2c3d4e5f") == 15
    assert day1.compute_oneline("treb7uchet") == 77


def test_day1():
    content = day1.get_file("2023-aoc-datasets/day1-sample.txt")
    result = day1.compute(content)
    assert result == 142

def test_day1b_compute():
    assert day1b.compute_oneline("two1nine") == 29
    assert day1b.compute_oneline("eightwothree") == 83
    assert day1b.compute_oneline("abcone2threexyz") == 13
    assert day1b.compute_oneline("xtwone3four") == 24
    assert day1b.compute_oneline("4nineeightseven2") == 42
    assert day1b.compute_oneline("zoneight234") == 14
    assert day1b.compute_oneline("7pqrstsixteen") == 76


