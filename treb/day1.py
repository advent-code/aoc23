#! /usr/bin/env python3
import sys
import argparse
import re

from typing import List



class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


def main(argv=None):
    if argv is None:
        argv = sys.argv

    parser = argparse.ArgumentParser(description='')
    parser.add_argument('fname', metavar='fname', type=str,
                        default=None, nargs='?',
                        help='Filename to read.')
    args = parser.parse_args()
    contents = get_file(args.fname)
    total = compute(contents)
    print(total)
    return total


def compute_oneline(line: str) -> int:
    matches = re.findall(r'\d+', line)
    digits = "".join(matches)
    result = int(f"{digits[0]}{digits[-1]}")
    return result


def compute(lines):
    total = 0
    for l in lines:
        total = total + compute_oneline(l)

    return total

def get_file(filename: str) -> List[str]:
    with open(filename) as f:
        return f.readlines()

if __name__ == "__main__":
    sys.exit(main())