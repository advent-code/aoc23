#! /usr/bin/env python3
import sys
import argparse
import re

from typing import List

DIGITNAMES = {"zero": "0",
              "one": "1",
              "two": "2",
              "three": "3",
              "four": "4",
              "five": "5",
              "six": "6",
              "seven": "7",
              "eight": "8",
              "nine": "9"}


class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


def main(argv=None):
    if argv is None:
        argv = sys.argv

    parser = argparse.ArgumentParser(description='')
    parser.add_argument('fname', metavar='fname', type=str,
                        default=None, nargs='?',
                        help='Filename to read.')
    args = parser.parse_args()
    contents = get_file(args.fname)
    total = compute(contents)
    print(total)
    return total


def walk_forward(line: str) -> int:
    start = 0
    while start <= len(line) - 1:
        # If the current pointer is a digit, don't try to replace it
        if line[start] in {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"}:
            return int(line[start])
        else:
            for d, v in DIGITNAMES.items():
                if line[start:].startswith(d):
                    return int(v)
            start = start + 1


def walk_backward(line: str) -> str:
    start = len(line) - 1
    while start >= 0:
        # If the current pointer is a digit, don't try to replace it
        if line[start] in {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"}:
            return line[start]
        else:
            for d, v in DIGITNAMES.items():
                if line[start:].startswith(d):
                    return v
            start = start - 1


def compute_oneline(line: str) -> int:
    line = line.strip()
    first = walk_forward(line)
    last = walk_backward(line)
    print(f"{line}: {first}{last}")
    result = int(f"{first}{last}")
    return result


def compute(lines):
    total = 0
    for l in lines:
        total = total + compute_oneline(l)

    return total


def get_file(filename: str) -> List[str]:
    with open(filename) as f:
        return f.readlines()


if __name__ == "__main__":
    sys.exit(main())
